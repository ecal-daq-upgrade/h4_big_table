# Based on https://github.com/FreeOpcUa/python-opcua/blob/master/examples/client-minimal.py
from opcua import Client
from opcua import ua

def browse_recursive(client, node, counter=0):
    mycounter = counter + 1
    for childId in node.get_children():
        ch = client.get_node(childId)
        if ch.get_node_class() == ua.NodeClass.Object:
            print("######################{}###################".format(ch.get_browse_name()))
            browse_recursive(client, ch, mycounter)
        elif ch.get_node_class() == ua.NodeClass.Variable:
            try:
                spaces = ""
                for i in range(mycounter):
                    spaces = spaces + " "
                print(spaces+"{bn} has value {val}".format(
                    bn=ch.get_browse_name(),
                    val=str(ch.get_value()))
                )
            except Exception as e:
                pass


def main():

    client = Client(url='opc.tcp://128.141.149.241:4840')
    try:
        client.connect()
        root = client.get_root_node()
        print("Root node is: ", root)
        objects = client.get_objects_node()
        print("Objects node is: ", objects)
        print("Children of root are: ", root.get_children())

        print('ActualPosition:',client.get_node(ua.NodeId.from_string('ns=3;s="ActualPosition"')).get_value())
        print('RunningSequence:',client.get_node(ua.NodeId.from_string('ns=3;s="RunningSequence"')).get_value())

        print('ParamRdWrCommand:',client.get_node(ua.NodeId.from_string('ns=3;s="ParamRdWrCommand"')).get_value())
        print('ParamIndex:',client.get_node(ua.NodeId.from_string('ns=3;s="ParamIndex"')).get_value())

        params = ua.ReadParameters()
        for node_id_str in ['ns=3;s="ParamIndex"', 'ns=3;s="ParamRdWrCommand"', 'ns=3;s="ParamNummer1"']:
            nodeid = ua.NodeId.from_string(node_id_str)
            attr = ua.ReadValueId()
            attr.NodeId = nodeid
            attr.AttributeId = ua.AttributeIds.Value
            params.NodesToRead.append(attr)

        results = client.uaclient.read(params)
        print("Results:")
        for result in results:
            print(result)

        print("This method to get the list of parents doesn't work")
        list_of_nodes = client.get_node(ua.NodeId.from_string('ns=3;s="ParamIndex"')).get_path()
        print("List length: ", len(list_of_nodes))
        for one_node in list_of_nodes:
            print(one_node.get_browse_name().to_string())
        print("Better to find another one")


        print("Second method")
        child_node = client.get_node(ua.NodeId.from_string('ns=3;s="ParamIndex"'))
        print(type(child_node))
        parent_node = child_node.get_parent()
        print(type(parent_node))
        print(parent_node.get_browse_name().to_string())

    except Exception as e:
        print(e)
    finally:
        client.disconnect()

if __name__ == '__main__':
    main()
