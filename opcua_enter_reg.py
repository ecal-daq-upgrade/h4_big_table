# Based on https://github.com/FreeOpcUa/python-opcua/blob/master/examples/client-minimal.py
from opcua import Client
from opcua import ua
import datetime
import time
import ctypes

def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val                         # return positive value as is

def main():

    client = Client(url='opc.tcp://128.141.149.241:4840')
    try:
        client.connect()
        root = client.get_root_node()
        print("Root node is: ", root)
        objects = client.get_objects_node()
        print("Objects node is: ", objects)
        print("Children of root are: ", root.get_children())
        print("\n")

        print("We need to be in read mode first --> it means ParamRdWrCommand set to 1")
        param_rd_wr_command_node = client.get_node(ua.NodeId.from_string('ns=3;s="ParamRdWrCommand"'))
        print('ParamRdWrCommand: ', param_rd_wr_command_node.get_value())
        print("Which class is ParamRdWrCommand? ", param_rd_wr_command_node.get_node_class())
        print("It is a Varaible so we can set a new value")
        print("Even if it was already equal to 1m we set its value to 1")
        dv = ua.DataValue(ua.Variant(1, ua.VariantType.Byte))
        param_rd_wr_command_node.set_value(dv)
        print("Reading again ParamRdWrCommand", param_rd_wr_command_node.get_value())
        print("\n")

        time.sleep(1)

        print("ParamIndex has to be set to 0")
        param_index_node = client.get_node(ua.NodeId.from_string('ns=3;s="ParamIndex"'))
        print("Checking its value... ParamIndex:", param_index_node.get_value())
        print("We set it equal to 0 even if it is already 0")
        dv = ua.DataValue(ua.Variant(0, ua.VariantType.Byte))
        param_index_node.set_value(dv)
        print("Reading again ParamIndex", param_index_node.get_value())
        print("\n")

        time.sleep(1)

        print("From the manual of Profibus, the Motor Position is at reg 845. So we set this value in ParamNummer1")
        param_nummer_1_node = client.get_node(ua.NodeId.from_string('ns=3;s="ParamNummer1"'))
        print("Which class is ParamNummer1? ", param_nummer_1_node.get_node_class())
        print("Checking its value... ParamNummer1:", param_nummer_1_node.get_value())
        print("ParamNummer1 is type WORD. It can be replaced by a UInt16 VariantType")
        dv = ua.DataValue(ua.Variant(845, ua.VariantType.UInt16))
        param_nummer_1_node.set_value(dv)
        print("Reading again ParamNummer1", param_nummer_1_node.get_value())
        print("\n")

        time.sleep(1)

        print("Now we can trigger a read of the register from Profibus setting to 1 StartParamFunction")
        start_param_function_node = client.get_node(ua.NodeId.from_string('ns=3;s="StartParamFunction"'))
        dv = ua.DataValue(ua.Variant(True, ua.VariantType.Boolean))
        start_param_function_node.set_value(dv)

        time.sleep(1)

        print("Now we can read the value from ParamValue1")
        param_value_1_node = client.get_node(ua.NodeId.from_string('ns=3;s="ParamValue1"'))

        lv = param_value_1_node.get_value()
        print("Checking its value, applying first the conversion of two's complement... ParamValue1:", twos_comp(lv,32))
        print("\n\n")




    except Exception as e:
        print(e)
    finally:
        client.disconnect()

if __name__ == '__main__':
    main()
