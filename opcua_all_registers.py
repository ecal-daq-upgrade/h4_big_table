# Based on https://github.com/FreeOpcUa/python-opcua/blob/master/examples/client-minimal.py
from opcua import Client
from opcua import ua

parent_child_list = []
parent_object_list = []

def browse_recursive(client, node, counter=0):

    mycounter = counter + 1
    for childId in node.get_children():
        ch = client.get_node(childId)
        if ch.get_node_class() == ua.NodeClass.Object:
            print("######################{}###################".format(ch.get_browse_name()))
            browse_recursive(client, ch, mycounter)
            parent_object_list.append({
                    'object_bn':ch.get_browse_name().to_string(),
                    'parent_id':node.get_browse_name().to_string()
                })
        elif ch.get_node_class() == ua.NodeClass.Variable:
            try:
                spaces = ""
                for i in range(mycounter):
                    spaces = spaces + " "
#                print(spaces+"{bn} with ID {id} has value {val} and NodeId {nid}".format(
#                    bn=ch.get_browse_name().to_string(),
#                    id=childId,
#                    val=str(ch.get_value()),
#                    nid=ch.get_data_type())
#                )

                name_string=ch.get_browse_name().to_string()
                print(name_string)
                single_node = {
                    'bn':name_string,
                    'id':str(childId),
                    'value':str(ch.get_value()),
                    'NodeId':str(ch.get_data_type()),
                    'parent_id':node.get_browse_name().to_string()
                }
                parent_child_list.append(single_node)

            except Exception as e:
                pass

def main():

    client = Client(url='opc.tcp://128.141.149.241:4840')
    try:
        client.connect()
        root = client.get_root_node()
        print("Root node is: ", root)
        objects = client.get_objects_node()
        print("Objects node is: ", objects)
        print("Children of root are: ", root.get_children())

        struct = client.get_node("i=85")
        client.load_type_definitions()
        browse_recursive(client, root)

        print("")


        search_list = ["3:ParamIndex", "3:Memory", "3:PLC_1", "2:DeviceSet", "0:Objects"]
        for node in parent_child_list:
            #if node["id"].find("ParamIndex") != -1:
            #    print(node)
            if node["bn"] in search_list:
                print(node)
        for node in parent_object_list:
            if node["object_bn"] in search_list:
                print(node)

        for node in parent_child_list:
            if node["bn"].find("CountValue") != -1:
                print("CountValue", node)


    except Exception as e:
        print(e)
    finally:
        client.disconnect()

if __name__ == '__main__':
    main()
